#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import time

from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix, roc_curve, auc, roc_auc_score
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, LeaveOneOut
from sklearn.model_selection import cross_val_score, cross_val_predict


def conclude_cm(cm):
    print('confusion matrix:\n', cm)

    [tp, fn], [fp, tn] = cm
    accuracy = (tp + tn) / (tp + fn + fp + tn)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    specificity = tn / (tn + fp)
    f1_score = 2 * precision * recall / (precision + recall)

    print('accuracy: %0.3f' % accuracy)
    print('precision: %0.3f' % precision)
    print('recall(aka sensitivity): %0.3f' % recall)
    print('specificity: %0.3f' % specificity)
    print('f1 score: %0.3f' % f1_score)


# load data
df = pd.read_csv('https://gitlab.com/yuxuan.chen/storage/'\
    '-/raw/master/MLDS-2019/week6/winequality-red.csv')
print(df.head())

# aggregate the numerical 'quality' into two classes
# here we use the threshold=5
# the new partition of quality replaces the older
bins = (2, 5, 8)
group_names = ['bad', 'good']
df['quality'] = pd.cut(x=df['quality'], 
                       bins = bins, 
                       labels = group_names)
sns.countplot(df['quality'])

# re-encode categorical columns with numerics
df['quality'] = LabelEncoder().fit_transform(df['quality'])
print(df['quality'].value_counts())

# correlation heatmap
f,ax=plt.subplots(figsize = (18,15))
sns.heatmap(data=df.corr(), 
            annot=True, 
            linewidths=0.5, 
            fmt = '.2f', 
            ax=ax)
plt.xticks(rotation=90)
plt.yticks(rotation=0)
plt.title('Correlation Heatmap')
plt.show()

X, y = df.drop('quality', axis=1), df['quality']


# 70% train, 30% test
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size = 0.3, random_state = 17)

start_time = time.time()
nb = GaussianNB().fit(X_train, y_train)
pred = nb.predict(X_test)
end_time = time.time()

pred_proba = nb.predict_proba(X_test)[:, 1]
fpr_1, tpr_1, _ = roc_curve(y_test,  pred_proba)
auc_1 = roc_auc_score(y_test, pred_proba)

cm = confusion_matrix(y_test, pred)
conclude_cm(cm)

run_time = end_time - start_time
print('running time: %s seconds' % run_time)

# reduce columns based on correlation
# remove 'pH' and 'fixed acidity'
X_train = X_train.drop(columns=['pH', 'fixed acidity'])
X_test = X_test.drop(columns=['pH', 'fixed acidity'])

nb = GaussianNB().fit(X_train, y_train)
pred = nb.predict(X_test)

pred_proba = nb.predict_proba(X_test)[:, 1]
fpr_3, tpr_3, _ = roc_curve(y_test,  pred_proba)
auc_3 = roc_auc_score(y_test, pred_proba)

cm = confusion_matrix(y_test, pred)

plt.plot(fpr_1, tpr_1, 
         label='Naive Bayes, auc=' + str(auc_1))
plt.plot(fpr_3, tpr_3, 
         label='Naive Bayes with feature selection, auc=' + str(auc_3))
plt.legend(loc=4)
plt.show()

# naive bayes with 5-fold cross-validation
start_time = time.time()
nb = GaussianNB()
pred = cross_val_predict(nb, X, y, cv=5)
end_time = time.time()

cm = confusion_matrix(y, pred)
conclude_cm(cm)

run_time = end_time - start_time
print('running time: %s seconds' % run_time)


# naive bayes with leave-one-out cross-validation
start_time = time.time()
nb = GaussianNB()
pred = cross_val_predict(nb, X, y, cv=LeaveOneOut().split(X))
end_time = time.time()

cm = confusion_matrix(y, pred)
conclude_cm(cm)

run_time = end_time - start_time
print('running time: %s seconds' % run_time)


# knn with 70-30
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size = 0.3, random_state = 17)

for k in range(1, 9):
    start_time = time.time()
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)
    pred_knn = knn.predict(X_test)
    end_time = time.time()

    print('=' * 50, '\nk=', k, ':')
    cm = confusion_matrix(y_test, pred_knn)
    conclude_cm(cm)

    run_time = end_time - start_time
    print('running time: %s seconds' % run_time)

# plot roc graph with the best k
knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(X_train, y_train)

pred_proba = knn.predict_proba(X_test)[:, 1]
fpr_2, tpr_2, _ = roc_curve(y_test,  pred_proba)
auc_2 = roc_auc_score(y_test, pred_proba)

plt.plot(fpr_1, tpr_1, label='Naive Bayes, auc='+str(auc_1))
plt.plot(fpr_2, tpr_2, label='KNN, auc='+str(auc_2))
plt.legend(loc=4)
plt.show()

# knn with 5-fold cross-validation
start_time = time.time()
knn = KNeighborsClassifier()
pred = cross_val_predict(knn, X, y, cv=5)
end_time = time.time()

cm = confusion_matrix(y, pred)
conclude_cm(cm)

run_time = end_time - start_time
print('running time: %s seconds' % run_time)


# knn with leave-one-out cross-validation
start_time = time.time()
knn = KNeighborsClassifier()
pred = cross_val_predict(knn, X, y, cv=LeaveOneOut().split(X))
end_time = time.time()

cm = confusion_matrix(y, pred)
conclude_cm(cm)

run_time = end_time - start_time
print('running time: %s seconds' % run_time)

# add noise
# 70% train, 30% test
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size = 0.3, random_state = 17)

dim = X_train.shape

for mu in [0.1, 0.2]:
    print('\nnoise level:', mu)
    noise = np.random.normal(scale=mu, size=dim)
    X_train = X_train + noise

    nb = GaussianNB()
    nb.fit(X_train, y_train)
    pred = nb.predict(X_test)
    cm = confusion_matrix(y_test, pred)   
    conclude_cm(cm)

    knn = KNeighborsClassifier(n_neighbors=1)
    knn.fit(X_train, y_train)
    pred = knn.predict(X_test)
    cm = confusion_matrix(y_test, pred)
    conclude_cm(cm)