#!/usr/bin/env python
# coding: utf-8

import networkx as nx
import csv
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable


# build directed graph with edge-list
G = nx.read_edgelist(
    path='week11_fish_trade_edges.txt', 
    comments='#', 
    create_using=nx.DiGraph, 
    nodetype=int, 
    data=(('millions_of_dollars_A_to_B', float), ), 
    edgetype=float)

# plot the graph
pos = nx.spring_layout(G)
nx.draw(G, 
        pos=pos, 
        cmap = plt.get_cmap('rainbow'), 
        with_labels=True)

# read country list into a dict
mapping = {}
with open('week11_fish_trade_countries_key.txt', 'r') as f:
    for i, line in enumerate(f):
        # read from the secod line, skip the header
        if i >= 1:
            (key, val) = line.rstrip().split(None, 1)
            key, val = int(key), val[1:-1]
            mapping[int(key)] = val

# relabel the nodes with country names
G = nx.relabel_nodes(G, mapping)
num_nodes, num_edges = G.number_of_nodes(), G.number_of_edges()
largest_scc = max(nx.strongly_connected_components(G), 
                  key=len)
largest_scc_percent = len(largest_scc) / num_nodes * 100

print('The size of the largest SCC is {}.'.format(len(largest_scc)))
print('This SCC contains {:.2f}% of all the nodes.'.format(
    largest_scc_percent
))
distances = nx.shortest_path_length(G, source='Germany')
print(distances)

recip_ratio = nx.algorithms.reciprocity(G) * 100
print('reciprocity percentage: {:.3f}%'.format(recip_ratio))

in_degrees = G.in_degree(list(G))
in_degrees_sorted = sorted(in_degrees, 
                           key=lambda tup: tup[1], 
                           reverse=True)
print(list(in_degrees_sorted)[:10])

exports = G.out_degree(list(G), 
                       weight='millions_of_dollars_A_to_B')
exports = sorted(exports, 
                 key=lambda tup: tup[1], 
                 reverse=True)
print(list(exports)[:10])

out_degrees = G.out_degree(list(G))
out_in_diff = [(x[0], abs(y[1] - x[1])) \
    for x, y in zip(in_degrees, out_degrees)]
diff_sorted = sorted(out_in_diff, 
                     key=lambda tup: tup[1], 
                     reverse=True)
print(diff_sorted[:10])

Graphs = []
probs = [0.0, 0.2, 0.4, 0.5, 0.8, 1.0]
for p in probs:
    # at the beginning, add nodes and edges manually
    G = nx.Graph()
    G.add_nodes_from([0, 1, 2])
    G.add_edges_from([(0, 1), (1, 2), (2, 0)])

    for n in range(3, 20000):
        # construct a new node
        G.add_node(n)

        # randomly select an existing node
        o = np.random.randint(low=0, high=n)

        # add a new edge
        flag = np.random.binomial(n=1, p=p)
        if flag:
            G.add_edge(n, o)
        else:
            # randomly select one node from the neighbors
            neighbors = list(G.neighbors(o))           
            ot_loc = np.random.randint(low=0, high=len(neighbors))
            ot = neighbors[ot_loc]

            # add a new edge
            G.add_edge(n, ot)
    
    Graphs.append(G)

# plot the distribution
fig, ax = plt.subplots()
plt.title('degree distribution')
plt.xlabel('degree')
plt.ylabel('normalized count')

for p, G in zip(probs, Graphs):
    degrees = list(G.degree())
    plt.hist(degrees)

plt.legend()
plt.show()

# build directed graph with edge-list from imdb
G = nx.read_weighted_edgelist(
    path='week11_imdb_actor_edges.tsv',  
    create_using=nx.Graph, 
    nodetype=int)

# read tsv. file for actors
df = pd.read_csv('week11_imdb_actors_key.tsv', 
                 sep='\t', 
                 encoding='iso-8859-1')
print(df.head())

# build id-name dict from the dataframe
mapping = dict(zip(df['ID'], df['name']))

# relabel the nodes with country names
G = nx.relabel_nodes(G, mapping)

# compute degree centrality
deg_cens = nx.degree_centrality(G)

# sort by degree centrality
deg_cen_sorted = [
    (k, v) for k, v in sorted(
        deg_cens.items(), 
        key=lambda item: - item[1])
]

# get the list of top-20 actors by degree centrality
deg_cen_actors = [actor for (actor, _) in deg_cen_sorted[:20]]
print(df.loc[df['name'].isin(deg_cen_actors)])

# get average of the number of films from the actors
avg1 = df.loc[df['name'].isin(deg_cen_actors)]['movies_95_04'].mean()
avg2 = df.loc[~df['name'].isin(deg_cen_actors)]['movies_95_04'].mean()

# print the results
print('Average of the numbers of the films from top-20 and others:')
print('{:.2f} vs {:.2f}'.format(avg1, avg2))

# compute betweenness centrality
bet_cens = nx.betweenness_centrality(G)

# sort by between centrality
bet_cen_sorted = [
    (k, v) for k, v in sorted(
        bet_cens.items(), 
        key=lambda item: - item[1])
]
print(list(bet_cen_sorted)[:20])

bet_cen_actors = [actor for (actor, _) in bet_cen_sorted[:20]]
print(df.loc[df['name'].isin(bet_cen_actors)])

# compute closeness centrality
cls_cens = nx.closeness_centrality(G)

# sort by closeness centrality
cls_cen_sorted = [
    (k, v) for k, v in sorted(
        cls_cens.items(), 
        key=lambda item: - item[1])
]
print(list(cls_cen_sorted)[:20])

cls_cen_actors = [actor for (actor, _) in cls_cen_sorted[:20]]
print(df.loc[df['name'].isin(cls_cen_actors)])