#!/usr/bin/env python
# coding: utf-8

import os
import time
import json
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy import stats
from pylab import rcParams
import sklearn
from sklearn.utils import check_random_state
from sklearn.datasets import load_digits
from sklearn.datasets import fetch_openml
from sklearn.preprocessing import scale
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.svm import LinearSVC, SVC
from sklearn import metrics
from sklearn.metrics import f1_score
from sklearn.metrics import pairwise_distances_argmin_min
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

max_queried = 500

def download(dataset='mnist_784'):
    if dataset == 'mnist_784':
        mnist = fetch_openml(dataset)
        X = mnist.data.astype('float64')
        y = mnist.target
        print ('MNIST:', X.shape, y.shape)
    else:
        wisconsin = sklearn.datasets.load_breast_cancer()
        X = wisconsin.data.astype('float64')
        y = wisconsin.target
        print ('breast cancer Wisconsin:', X.shape, y.shape)
    return (X, y)


def split(train_size=60000):
    X_train_full = X[:train_size]
    y_train_full = y[:train_size]
    X_test = X[train_size:]
    y_test = y[train_size:]
    return (X_train_full, y_train_full, X_test, y_test)


class BaseModel(object):

    def __init__(self):
        pass

    def fit_predict(self):
        pass


class SvmModel(BaseModel):

    model_type = 'Support Vector Machine with Linear Kernel'
    
    def fit_predict(self, X_train, y_train, X_val, X_test, c_weight):
        print ('training SVM...')
        self.classifier = SVC(
            C=1, 
            kernel='linear', 
            probability=True, 
            class_weight=c_weight)
        self.classifier.fit(X_train, y_train)
        self.test_y_predicted = self.classifier.predict(X_test)
        self.val_y_predicted = self.classifier.predict(X_val)
        
        return (X_train, X_val, X_test, 
                self.val_y_predicted, self.test_y_predicted)


class NbModel(BaseModel):

    model_type = 'Naive Bayes Classifier' 
    
    def fit_predict(self, X_train, y_train, X_val, X_test, c_weight):
        print ('training Naive Bayes')
        self.classifier = GaussianNB()
        self.classifier.fit(X_train, y_train)
        self.test_y_predicted = self.classifier.predict(X_test)
        self.val_y_predicted = self.classifier.predict(X_val)
        
        return (X_train, X_val, X_test, 
                self.val_y_predicted, self.test_y_predicted)


class RfModel(BaseModel):

    model_type = 'Random Forest'
    
    def fit_predict(self, X_train, y_train, X_val, X_test, c_weight):
        print('training Random Forest...')
        self.classifier = RandomForestClassifier(
            n_estimators=500, 
            class_weight=c_weight)
        self.classifier.fit(X_train, y_train)
        self.test_y_predicted = self.classifier.predict(X_test)
        self.val_y_predicted = self.classifier.predict(X_val)
        
        return (X_train, X_val, X_test, 
                self.val_y_predicted, self.test_y_predicted)


class TrainModel:

    def __init__(self, model_object):        
        self.accuracies = []
        self.f1_scores = []
        self.model_object = model_object()        

    def print_model_type(self):
        print (self.model_object.model_type)

    # we train normally and get probabilities for the validation set. 
    # i.e., we use the probabilities to select the most uncertain samples

    def train(self, X_train, y_train, X_val, X_test, c_weight):
        print('Train set:', X_train.shape, 'y:', y_train.shape, end='')
        print('Val   set:', X_val.shape, end='')
        print('Test  set:', X_test.shape)
        t0 = time.time()
        
        (X_train, X_val, X_test, 
        self.val_y_predicted, self.test_y_predicted) \
            = self.model_object.fit_predict(
                X_train, y_train, X_val, X_test, c_weight)
        
        self.run_time = time.time() - t0
        # we return them only in case we use PCA
        return (X_train, X_val, X_test)

    # we want accuracy only for the test set
    def get_test_accuracy(self, i, y_test):

        accuracy = np.mean(self.test_y_predicted.ravel() == y_test.ravel())
        f1score = f1_score(y_test, self.test_y_predicted, average='weighted')
        f1score = float("{:.3f}".format(float(f1score)))
        self.accuracies.append(accuracy)
        self.f1_scores.append(f1score)

        print('--------------------------------')
        print('Iteration:', i)
        print('Example run in {:.3f} s'.format(self.run_time))
        print('Accuracy rate: {:.3f}%, F1-score: {}'.format(
            accuracy * 100, f1score))    
        print('--------------------------------')


class BaseSelectionFunction(object):

    def __init__(self):
        pass

    def select(self):
        pass


class RandomSelection(BaseSelectionFunction):

    @staticmethod
    def select(probas_val, initial_labeled_samples):
        random_state = check_random_state(0)
        selection = np.random.choice(
            probas_val.shape[0], initial_labeled_samples, replace=False)
        return selection


class EntropySelection(BaseSelectionFunction):

    @staticmethod
    def select(probas_val, initial_labeled_samples):
        entropy = (-probas_val * np.log2(probas_val)).sum(axis=1)
        selection = (np.argsort(entropy)[::-1])[:initial_labeled_samples]
        return selection
      
      
class MarginSamplingSelection(BaseSelectionFunction):

    @staticmethod
    def select(probas_val, initial_labeled_samples):
        rev = np.sort(probas_val, axis=1)[:, ::-1]
        values = rev[:, 0] - rev[:, 1]
        selection = np.argsort(values)[:initial_labeled_samples]
        return selection


class Normalize(object):
    
    def normalize(self, X_train, X_val, X_test):
        self.scaler = MinMaxScaler()
        X_train = self.scaler.fit_transform(X_train)
        X_val   = self.scaler.transform(X_val)
        X_test  = self.scaler.transform(X_test)
        return (X_train, X_val, X_test) 
    
    def inverse(self, X_train, X_val, X_test):
        X_train = self.scaler.inverse_transform(X_train)
        X_val   = self.scaler.inverse_transform(X_val)
        X_test  = self.scaler.inverse_transform(X_test)
        return (X_train, X_val, X_test)


def get_k_random_samples(
    initial_labeled_samples, X_train_full, y_train_full):
    random_state = check_random_state(0)
    permutation = np.random.choice(
        X_train_full.shape[0], 
        size=initial_labeled_samples, 
        replace=False)
    print('initial random chosen samples', permutation.shape)

    X_train = X_train_full[permutation]
    y_train = y_train_full[permutation]

    X_train = X_train.reshape((X_train.shape[0], -1))
    bin_count = np.bincount(y_train.astype('int64'))
    unique = np.unique(y_train.astype('int64'))
    print(
        'initial train set:', X_train.shape, y_train.shape,
        'unique(labels):', bin_count, unique)
    return (permutation, X_train, y_train)


class TheAlgorithm(object):

    accuracies = []

    def __init__(self, initial_labeled_samples, model_object, selection_function):
        self.initial_labeled_samples = initial_labeled_samples
        self.model_object = model_object
        self.sample_selection_function = selection_function

    def run(self, X_train_full, y_train_full, X_test, y_test):

        (permutation, X_train, y_train) = \
            get_k_random_samples(self.initial_labeled_samples,
                                 X_train_full, y_train_full)
        self.queried = self.initial_labeled_samples
        self.samplecount = [self.initial_labeled_samples]

        # assign the val set the rest of the 'unlabelled' training data
        X_val = np.array([])
        y_val = np.array([])
        X_val = np.copy(X_train_full)
        X_val = np.delete(X_val, permutation, axis=0)
        y_val = np.copy(y_train_full)
        y_val = np.delete(y_val, permutation, axis=0)
        print('val set:', X_val.shape, y_val.shape, permutation.shape)

        # normalize data
        normalizer = Normalize()
        X_train, X_val, X_test = normalizer.normalize(X_train, X_val, X_test)      
        self.clf_model = TrainModel(self.model_object)
        (X_train, X_val, X_test) = \
            self.clf_model.train(X_train, y_train, X_val, X_test, 'balanced')
        active_iteration = 1
        self.clf_model.get_test_accuracy(1, y_test)

        while self.queried < max_queried:
            active_iteration += 1
            # get validation probabilities
            probas_val = self.clf_model.model_object.classifier.predict_proba(X_val)

            uncertain_samples = self.sample_selection_function.select(
                    probas_val, self.initial_labeled_samples)
 
            X_train, X_val, X_test = normalizer.inverse(X_train, X_val, X_test)   

            # get the uncertain samples from the validation set
            print('trainset before', X_train.shape, y_train.shape, end='')
            X_train = np.concatenate((X_train, X_val[uncertain_samples]))
            y_train = np.concatenate((y_train, y_val[uncertain_samples]))
            print('trainset after', X_train.shape, y_train.shape)
            self.samplecount.append(X_train.shape[0])

            bin_count = np.bincount(y_train.astype('int64'))
            unique = np.unique(y_train.astype('int64'))
            print('updated train set:', X_train.shape, y_train.shape, 
            'unique(labels):', bin_count, unique)

            X_val = np.delete(X_val, uncertain_samples, axis=0)
            y_val = np.delete(y_val, uncertain_samples, axis=0)
            print('val set:', X_val.shape, y_val.shape)

            # normalize again after creating the 'new' train/test sets
            normalizer = Normalize()
            X_train, X_val, X_test = normalizer.normalize(X_train, X_val, X_test)               

            self.queried += self.initial_labeled_samples
            (X_train, X_val, X_test) = \
                self.clf_model.train(
                    X_train, y_train, X_val, X_test, 'balanced')
            self.clf_model.get_test_accuracy(active_iteration, y_test)

        print('final active learning accuracies', self.clf_model.accuracies)

(X, y) = download('mnist_784')
(X_train_full, y_train_full, X_test, y_test) = split(60000)
print ('train:', X_train_full.shape, y_train_full.shape, end='  ')
print ('test :', X_test.shape, y_test.shape, end='  ')
classes = len(np.unique(y))
print ('unique classes', classes)

def pickle_save(fname, data):
    filehandler = open(fname, 'wb')
    pickle.dump(data,filehandler)
    filehandler.close() 
    print('saved', fname, os.getcwd(), os.listdir())


def pickle_load(fname):
    print(os.getcwd(), os.listdir())
    file = open(fname, 'rb')
    data = pickle.load(file)
    file.close()
    print(data)
    return data


def experiment(d, models, selection_functions, Ks, countfrom):
    print ('stopping at:', max_queried)
    count = 0

    for model_object in models:
      if model_object.__name__ not in d:
          d[model_object.__name__] = {}
      
      for selection_function in selection_functions:
        if selection_function.__name__ not in d[model_object.__name__]:
            d[model_object.__name__][selection_function.__name__] = {}
        
        for k in Ks:
            d[model_object.__name__][selection_function.__name__][str(k)] = []                     
            count += 1
            if count >= countfrom:
                print (
                    'Count = {}, using model = {}, selection_function = {}, k = {}'\
                    .format(count, model_object.__name__, selection_function.__name__, k)
                )
                alg = TheAlgorithm(k, model_object, selection_function)
                alg.run(X_train_full, y_train_full, X_test, y_test)
                d[model_object.__name__][selection_function.__name__][str(k)].append(
                    (alg.clf_model.accuracies, alg.clf_model.f1_scores))
                fname = 'Active-learning-experiment-' + str(count) + '.pkl'
                pickle_save(fname, d)
                if count % 5 == 0:
                    print(json.dumps(d, indent=2, sort_keys=True))
                print ('\n----------FINISHED--------------\n')
    return d

max_queried = 500 
models = [NbModel, SvmModel, RfModel] 
selection_functions = [
    RandomSelection, 
    MarginSamplingSelection, 
    EntropySelection] 
Ks = [10, 50, 100, 250] 

d = {}
stopped_at = -1 

d = experiment(d, models, selection_functions, Ks, stopped_at+1)
print(d)
results = json.loads(json.dumps(d, indent=2, sort_keys=True))
print(results)

with open('results.json', 'w') as f:
    json.dump(results, f)

mnist_ressults = {}
with open('results.json', 'r') as f:
    mnist_results = json.load(f)
    print(mnist_results)

models_str = ['NbModel', 'SvmModel', 'RfModel']
selection_functions_str = [
    'RandomSelection', 
    'MarginSamplingSelection', 
    'EntropySelection']
Ks_str = ['10','50','100','250']

total_experiments = \
    len(models_str) * len(selection_functions_str) * len(Ks_str)

# compute the supervised accuracy, f1-score
supervised_accuracies, supervised_f1_scores = {}, {}

for model_name in ['NbModel']:
    if model_name == 'NbModel':
        classifier = GaussianNB()
#     elif model_name == 'SvmModel':
#         classifier = SVC(kernel='linear', 
#                          probability=True, 
#                          class_weight='balanced')
#     elif model_name == 'RfModel':
#         classifier = RandomForestClassifier(
#             n_estimators=500, class_weight='balanced')
    
        # predict
        classifier.fit(X_train_full, y_train_full)
        test_y_predicted = classifier.predict(X_test)

        accuracy = np.mean(test_y_predicted.ravel() == y_test.ravel())
        f1score = f1_score(y_test, test_y_predicted, average='weighted')

        supervised_accuracies[model_name] = accuracy
        supervised_f1_scores[model_name] = f1score

print(supervised_accuracies, supervised_f1_scores)


def performance_plot(supervised_accuracy, dic, models, 
                     selection_functions, Ks, metric='accuracy'):  
    fig, ax = plt.subplots()
    ax.plot([0,500],
            [supervised_accuracy, supervised_accuracy], 
            label = 'algorithm-upper-bound')
    
    metric_id = 0 if metric == 'accuracy' else 1
    for model_object in models:
        for selection_function in selection_functions:
            for idx, k in enumerate(Ks):
                x = np.arange(float(Ks[idx]), 500 + float(Ks[idx]), float(Ks[idx]))            
                y = np.array(dic[model_object][selection_function][k][0][metric_id])
                y = 100 * y
                label_ = model_object + '-' + selection_function + '-' + str(k)
                plt.plot(x, y, label=label_)
    ax.legend()
    ax.set_xlim([50,500])
    ax.set_ylim([40,100])
    ax.grid(True)
    plt.show()

# [nb_upper_bound, svm_upper_bound, rf_upper_bound]\
#     = supervised_accuracies

nb_upper_bound = 55.68
random_forest_upper_bound = 97.
svm_upper_bound = 94.

print('So which is the better model? under the stopping condition' 
      'and hyper parameters - random forest is the winner!')
performance_plot(
    nb_upper_bound, d, ['NbModel'], 
    selection_functions_str, Ks_str, 'accuracy')
performance_plot(
    random_forest_upper_bound, d, ['RfModel'], 
    selection_functions_str, Ks_str, 'accuracy')
performance_plot(
    svm_upper_bound, d, ['SvmModel'], 
    selection_functions_str, Ks_str, 'accuracy')

print('So which is the best sample selection function?'
      ' margin sampling is the winner!')
performance_plot(
    random_forest_upper_bound, d, ['RfModel'], 
    selection_functions_str, Ks_str, 'accuracy')

print('So which is the best k? k=10 is the winner')
performance_plot(
    random_forest_upper_bound, d, ['RfModel'], 
    ['MarginSamplingSelection'], Ks_str, 'accuracy')


nb_upper_bound = 51.7
random_forest_upper_bound = 0
svm_upper_bound = 0

print('So which is the better model? under the stopping condition'
      'and hyper parameters - random forest is the winner!')
performance_plot(
    nb_upper_bound, d, ['NbModel'], 
    selection_functions_str, Ks_str, 'f1-score')
performance_plot(
    random_forest_upper_bound, d, ['RfModel'], 
    selection_functions_str, Ks_str, 'f1-score')
performance_plot(
    svm_upper_bound, d, ['SvmModel'], 
    selection_functions_str, Ks_str, 'f1-score')

print('So which is the best sample selection' 
      'function? margin sampling is the winner!')
performance_plot(
    random_forest_upper_bound, d, ['RfModel'], 
    selection_functions_str, Ks_str, 'f1-score'
)

print('So which is the best k? k=10 is the winner')
performance_plot(
    random_forest_upper_bound, d, ['RfModel'], 
    ['MarginSamplingSelection'], Ks_str, 'f1-score')


(X, y) = download('iris')
(X_train_full, y_train_full, X_test, y_test) = split(400)
print ('train:', X_train_full.shape, y_train_full.shape, end='  ')
print ('test :', X_test.shape, y_test.shape, end='  ')
classes = len(np.unique(y))
print ('unique classes', classes)

max_queried = 100 
models = [NbModel, SvmModel, RfModel] 
selection_functions = [
    RandomSelection, 
    MarginSamplingSelection, 
    EntropySelection] 
Ks = [5, 10, 20, 50]
Ks_str = [str(k) for k in Ks]

d_wis = {}
stopped_at = -1

d_wis = experiment(d_wis, models, selection_functions, Ks, stopped_at+1)
print(d_wis)

results_wis = json.loads(json.dumps(d_wis, indent=2, sort_keys=True))
print(results_wis)

with open('results_wisconsin.json', 'w') as f:
    json.dump(results_wis, f)

# compute the supervised accuracy, f1-score
supervised_accuracies_wis, supervised_f1_scores_wis = {}, {}

for model_name in models_str:
    if model_name == 'NbModel':
        classifier = GaussianNB()
    elif model_name == 'SvmModel':
        classifier = SVC(kernel='linear', 
                         probability=True, 
                         class_weight='balanced')
    elif model_name == 'RfModel':
        classifier = RandomForestClassifier(
            n_estimators=500, class_weight='balanced')
    
    # predict
    classifier.fit(X_train_full, y_train_full)
    test_y_predicted = classifier.predict(X_test)

    accuracy = np.mean(test_y_predicted.ravel() == y_test.ravel())
    f1score = f1_score(y_test, test_y_predicted, average='weighted')

    supervised_accuracies_wis[model_name] = accuracy
    supervised_f1_scores_wis[model_name] = f1score

print(supervised_accuracies_wis, supervised_f1_scores_wis)


def performance_plot_wis(supervised_accuracy, dic, models, 
                         selection_functions, Ks, metric='accuracy'):  
    fig, ax = plt.subplots()
    ax.plot([0,100],
            [supervised_accuracy, supervised_accuracy], 
            label = 'algorithm-upper-bound')
    
    metric_id = 0 if metric == 'accuracy' else 1
    for model_object in models:
        for selection_function in selection_functions:
            for idx, k in enumerate(Ks):
                x = np.arange(float(Ks[idx]), 100 + float(Ks[idx]), float(Ks[idx]))            
                y = np.array(dic[model_object][selection_function][k][0][metric_id])
                y = 100 * y
                label_ = model_object + '-' + selection_function + '-' + str(k)
                plt.plot(x, y, label=label_)
                
    ax.legend()
    ax.set_xlim([10,100])
    ax.set_ylim([70,100])
    ax.grid(True)
    plt.show()


nb_upper_bound_wis = 96.45
random_forest_upper_bound_wis = 96.45
svm_upper_bound_wis = 94.08

print('So which is the better model? under the stopping condition'
      'and hyper parameters - random forest is the winner!')
performance_plot_wis(
    nb_upper_bound_wis, d_wis, ['NbModel'], 
    selection_functions_str, Ks_str, 'accuracy')
performance_plot_wis(
    random_forest_upper_bound_wis, d_wis, ['RfModel'], 
    selection_functions_str, Ks_str, 'accuracy')
performance_plot_wis(
    svm_upper_bound_wis, d_wis, ['SvmModel'], 
    selection_functions_str, Ks_str, 'accuracy')


print('So which is the best sample selection' 
      'function? margin sampling is the winner!')
performance_plot_wis(
    random_forest_upper_bound, d_wis, ['RfModel'], 
    selection_functions_str, Ks_str, 'accuracy')

print('So which is the best k? k=10 is the winner')
performance_plot_wis(
    random_forest_upper_bound, d_wis, ['RfModel'], 
    ['MarginSamplingSelection'], Ks_str, 'accuracy')