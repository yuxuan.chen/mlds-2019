# -*- coding: utf-8 -*-
# TBAR implementation adapted from
# https://github.com/omjego/AR-Mining-Hash-Tree/blob/master/apriori_with_htree.py

!pip install efficient_apriori
from efficient_apriori import apriori
import csv, itertools
import datetime, hashlib

# download dataset
url = 'https://gitlab.com/yuxuan.chen/storage/'\
    '-/raw/master/MLDS-2019/week4/dataset.csv'
!rm dataset.*
!wget $url

def data_generator(filename):
    # Data generator, needs to return a generator to be called several times.
    def data_gen():
        with open(filename) as file:
            for line in file:
                yield tuple(k.strip() for k in line.split(','))      

    return data_gen


start = datetime.datetime.now()
transactions = data_generator('dataset.csv')
itemsets, rules = apriori(
    transactions, min_support=0.05, min_confidence=0.0)
end = datetime.datetime.now()
print ('runtime:', end - start)
print(rules)


def load_data(filename):
    reader = csv.reader(open(filename, 'r'), delimiter=',')
    trans = [map(str, row[:]) for row in reader]
    return trans


def find_frequent_one(data_set, support):
    candidate_one = {}
    total = len(data_set)
    for row in data_set:
        for item in row:
            if item in candidate_one:
                candidate_one[item] += 1
            else:
                candidate_one[item] = 1

    frequent_1 = []
    for key, cnt in candidate_one.items():
        if cnt >= (support * total / 100):
            frequent_1.append(([key], cnt))
    return frequent_1


class HNode:
    def __init__(self):
        self.children = {}
        self.isLeaf = True
        self.bucket = {}


class HTree:
    def __init__(self, max_leaf_cnt, max_child_cnt):
        self.root = HNode()
        self.max_leaf_cnt = max_leaf_cnt
        self.max_child_cnt = max_child_cnt
        self.frequent_itemsets = []

    def recur_insert(self, node, itemset, index, cnt):
        if index == len(itemset):
            if itemset in node.bucket:
                node.bucket[itemset] += cnt
            else:
                node.bucket[itemset] = cnt
            return

        if node.isLeaf:
            if itemset in node.bucket:
                node.bucket[itemset] += cnt
            else:
                node.bucket[itemset] = cnt
            if len(node.bucket) == self.max_leaf_cnt:
                for old_itemset, old_cnt in node.bucket.items():
                    hash_key = self.hash(old_itemset[index])
                    if hash_key not in node.children:
                        node.children[hash_key] = HNode()
                    self.recur_insert(
                        node.children[hash_key], old_itemset, 
                        index + 1, old_cnt)
                del node.bucket
                node.isLeaf = False
        else:
            hash_key = self.hash(itemset[index])
            if hash_key not in node.children:
                node.children[hash_key] = HNode()
            self.recur_insert(
                node.children[hash_key], itemset, 
                index + 1, cnt)

    def insert(self, itemset):
        itemset = tuple(itemset)
        self.recur_insert(self.root, itemset, 0, 0)

    def add_support(self, itemset):
        runner = self.root
        itemset = tuple(itemset)
        index = 0
        while True:
            if runner.isLeaf:
                if itemset in runner.bucket:
                    runner.bucket[itemset] += 1
                break
            hash_key = self.hash(itemset[index])
            if hash_key in runner.children:
                runner = runner.children[hash_key]
            else:
                break
            index += 1

    def dfs(self, node, support_cnt):
        if node.isLeaf:
            for key, value in node.bucket.items():
                if value >= support_cnt:
                    self.frequent_itemsets.append((list(key), value))
            return

        for child in node.children.values():
            self.dfs(child, support_cnt)

    def get_frequent_itemsets(self, support_cnt):
        self.frequent_itemsets = []
        self.dfs(self.root, support_cnt)
        return self.frequent_itemsets

    def hash(self, val):
        val = val.encode('utf-8')
        hash_object = hashlib.md5(val)
        return hash_object.hexdigest()


def generate_hash_tree(candidate_itemsets, length, 
                       max_leaf_cnt=4, max_child_cnt=5):
    htree = HTree(max_child_cnt, max_leaf_cnt)
    for itemset in candidate_itemsets:
        htree.insert(itemset)
    return htree


def generate_k_subsets(dataset, length):
    subsets = []
    for itemset in dataset:
        subsets.extend(
            map(list, itertools.combinations(itemset, length)))
    return subsets


def is_prefix(list_1, list_2):
    for i in range(len(list_1) - 1):
        if list_1[i] != list_2[i]:
            return False
    return True


def apriori_generate_frequent_itemsets(dataset, support):
    support_cnt = int(support / 100.0 * len(dataset))
    all_frequent_itemsets = find_frequent_one(dataset, support)
    prev_frequent = [x[0] for x in all_frequent_itemsets]
    length = 2
    while len(prev_frequent) > 1:
        new_candidates = []
        for i in range(len(prev_frequent)):
            j = i + 1
            while j < len(prev_frequent) and \
                is_prefix(prev_frequent[i], prev_frequent[j]):
                new_candidates.append(prev_frequent[i][:-1] +
                                      [prev_frequent[i][-1]] +
                                      [prev_frequent[j][-1]])
                j += 1

        h_tree = generate_hash_tree(new_candidates, length)
        k_subsets = generate_k_subsets(dataset, length)

        for subset in k_subsets:
            h_tree.add_support(subset)

        new_frequent = h_tree.get_frequent_itemsets(support_cnt)
        all_frequent_itemsets.extend(new_frequent)
        prev_frequent = [tup[0] for tup in new_frequent]
        prev_frequent.sort()
        length += 1

    return all_frequent_itemsets


def generate_association_rules(f_itemsets, confidence):

    hash_map = {}
    for itemset in f_itemsets:
        hash_map[tuple(itemset[0])] = itemset[1]

    a_rules = []
    for itemset in f_itemsets:
        length = len(itemset[0])
        if length == 1:
            continue

        union_support = hash_map[tuple(itemset[0])]
        for i in range(1, length):

            lefts = map(list, itertools.combinations(itemset[0], i))
            for left in lefts:
                conf = 100.0 * union_support / hash_map[tuple(left)]
                if conf >= confidence:
                    a_rules.append(
                        [left,list(set(itemset[0]) - set(left)), conf])
    return a_rules


def print_rules(rules):
    for item in rules:
        left = ','.join(map(str, item[0]))
        right = ','.join(map(str, item[1]))
        print (' ==> '.join([left, right]))
    print('Total Rules Generated: ', len(rules))


transactions = load_data('dataset.csv')
frequent = apriori_generate_frequent_itemsets(transactions, 0.0)
rules = generate_association_rules(frequent, 0.05)
print_rules(rules)