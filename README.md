# MLDS-2019 (Yuxuan Chen)

This is the repository of Yuxuan Chen's submissions for the 2019/20 WiSo course "Machine Learning in Data Science".

Contact info: yuxuan.chen@fu-berlin.de

## Table of Content

#### [Assignment Week 4](https://gitlab.com/yuxuan.chen/mlds-2019/-/tree/master/Assignment%20Week%204)  KDD
- [project_1.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%204/project_1.py) 
  -- code to be submitted for the project to compare APRIORI and the TBAR algorithm
- [report_1.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%204/report_1.pdf) 
  -- report to be submitted on the project
- [project_1.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%204/project_1.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment Week 5](https://gitlab.com/yuxuan.chen/mlds-2019/-/tree/master/Assignment%20Week%205)  Decision Trees
- [project_2.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%205/project_2.py) 
  -- code to be submitted for the project to study Decision Trees on Iris Dataset
- [report_2.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%205/report_2.pdf) 
  -- report to be submitted on the project
- [project_2.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%205/project_2.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment Week 6](https://gitlab.com/yuxuan.chen/mlds-2019/-/tree/master/Assignment%20Week%206)  Naive Bayes and kNN
- [project_3.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%206/project_3.py) 
  -- code to be submitted for the project to compare Naive Bayes and kNN for wine quality prediction
- [report_3.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%206/report_3.pdf) 
  -- report to be submitted on the project
- [project_3.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%206/project_3.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment Week 7](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%207)  k-Means and Hierarchical Clustering
- [project_7.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%207/project_7.py) 
  -- code to be submitted for the project to do KMeans and hierarchical clustering on toy data
- [report_7.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%207/report_7.pdf) 
  -- report to be submitted on the project
- [project_7.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%207/project_7.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment Week 8](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%208)  DBSCAN and SOM Clustering
- [project_8.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%208/project_8.py) 
  -- code to be submitted for the project to do DBSCAN and SOM clustering on toy data
- [report_8.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%208/report_8.pdf) 
  -- report to be submitted on the project
- [project_8.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%208/project_8.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment Week 9](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%209)  Image Classification with CNN
- [project_9.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%209/project_9.py) 
  -- code to be submitted for the project to CNN classification
- [report_9.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%209/report_9.pdf) 
  -- report to be submitted on the project
- [project_9.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%209/project_9.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment Week 11](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2011)  Network Analysis
- [project_11.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2011/project_11.py) 
  -- code to be submitted for the project of network analysis
- [report_11.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2011/report_11.pdf) 
  -- report to be submitted on the project
- [project_11.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2011/project_11.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment Week 13](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2013)  Active Learning
- [project_13.py](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2013/project_13.py) 
  -- code to be submitted for the project of active learning
- [report_13.pdf](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2013/report_13.pdf) 
  -- report to be submitted on the project
- [project_13.ipynb](https://gitlab.com/yuxuan.chen/mlds-2019/-/blob/master/Assignment%20Week%2013/project_13.ipynb)
  -- IPython Notebook version for the sumbitted Python file


## Dependencies
- **Python 3.5+**