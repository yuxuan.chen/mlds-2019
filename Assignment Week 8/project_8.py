#!/usr/bin/python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

from sklearn.decomposition import PCA
from sklearn.datasets.samples_generator import make_blobs, make_circles
from sklearn.cluster import KMeans, DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import kneighbors_graph
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics import homogeneity_score, completeness_score, adjusted_rand_score, silhouette_score

np.random.seed(0)


def dbscan_plot(data, model, scaling=False, title=''):
    # apply the model
    if scaling:
        data = StandardScaler().fit_transform(data)
    model.fit(data)
    pred = model.labels_
    
    X, Y = data[:, 0], data[:, 1]   
    plt.figure(figsize=(8, 6))
    h = 0.02
    x_min, x_max = X.min() - 0.3, X.max() + 0.3
    y_min, y_max = Y.min() - 0.3, Y.max() + 0.3      
    
    # plot data with class
    # plt.plot(X, Y, 'b.', markersize=6)
    plt.scatter(X, Y, c=pred, s=10, cmap='plasma')

    # Plot the centroids
    # centroids = model.cluster_centers_
    # plt.scatter(centroids[:, 0], centroids[:, 1],
    #             marker='x', s=50, linewidths=3,
    #             color='w', zorder=10)

    plt.title(title)
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.show()

    return pred


def kmeans_plot(data, model, scaling=False, title=''):
    # apply the model
    if scaling:
        data = StandardScaler().fit_transform(data)
    model.fit(data)

    # plot data
    X, Y = data[:, 0], data[:, 1]
    plt.figure(figsize=(8, 6))
    h = 0.02
    x_min, x_max = X.min() - 0.3, X.max() + 0.3
    y_min, y_max = Y.min() - 0.3, Y.max() + 0.3
    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # plot border
    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.figure(1)
    plt.clf()
    plt.imshow(Z, 
               interpolation='nearest',
               extent=(xx.min(), xx.max(), yy.min(), yy.max()),
               cmap=plt.cm.Paired,
               aspect='auto', origin='lower')
    
    # plot data with class
    pred = model.predict(data)
    # plt.plot(X, Y, 'b.', markersize=6)
    plt.scatter(X, Y, c=pred, s=10, cmap='plasma')
    

    # Plot the centroids
    centroids = model.cluster_centers_
    plt.scatter(centroids[:, 0], centroids[:, 1],
                marker='x', s=50, linewidths=3,
                color='w', zorder=10)

    plt.title(title)
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.show()


def kdist_plot(data, k=4, scaling=False):
    if scaling:
        data = StandardScaler().fit_transform(data)

    nbrs = NearestNeighbors(n_neighbors=k).fit(data)
    distances, indices = nbrs.kneighbors(data)
    kdist = sorted(distances[:, k-1], reverse=True)

    plt.plot(list(range(1, data.shape[0] + 1)), kdist)


def evaluate(data, true_labels, pred_labels, title=''):
    print('=' * 20, '\n', title)
    print('Homogeneity: %0.3f' % homogeneity_score(
        true_labels, pred_labels))
    print('Completeness: %0.3f' % completeness_score(
        true_labels, pred_labels))
    print('Adjusted Rand Index: %0.3f' % adjusted_rand_score(
        true_labels, pred_labels))

    if len(np.unique(pred)) < 2:
        print('Silhouette Score does not exist!')
    else:
        print('Silhouette Score: %0.3f' % silhouette_score(data, pred_labels))
    print('=' * 20)

'''
1. Extend the Data
    Generate 6 datasets:
        1. Non-Cluster Data
        2. Non-Spherical Data
        3. Many Clusters, close to each other
        4. Different Cluster Sizes
        5. Different Cluster Densities
        6. 3 Clusters after re-scaling
'''
# generate data_1: non-cluster data
data_1x = np.random.uniform(low=-4, high=4, size=200)
data_1y = np.random.uniform(low=-4, high=4, size=200)
data_1 = np.array([[x, y] for x, y in zip(data_1x, data_1y)])
# assign labels
labels_1 = [int(y >= 0) for y in data_1[:, 1]]
plt.figure(figsize=(8, 6))
plt.scatter(data_1[:, 0], data_1[:, 1], c='blue', s=10)

# generate data_2: non-spherical data
data_2, labels_2 = make_circles(n_samples=1500,
                               factor=0.5,
                               noise=0.05)
plt.figure(figsize=(8, 6))
plt.scatter(data_2[:, 0], data_2[:, 1], c='blue', s=10)

# generate data_3: many clusters, close to each other
data_3, labels_3 = make_blobs(n_samples=500, 
                       centers=4,
                       cluster_std=0.7, 
                       random_state=0)
plt.figure(figsize=(8, 6))
plt.scatter(data_3[:, 0], data_3[:, 1], c='blue', s=10)

# generate data_4: different cluster sizes
data_4, labels_4 = make_blobs(
    n_samples=[500, 100, 100], 
    centers=[[0, 0], [-3, 0], [3, 0]],
    cluster_std=[0.4, 0.2, 0.2], 
    random_state=0)
plt.figure(figsize=(8, 6))
plt.scatter(data_4[:, 0], data_4[:, 1], c='blue', s=10)

# generate data_5: different cluster densities
data_5, labels_5 = make_blobs(
    n_samples=[500, 30, 50], 
    centers=[[0, 0], [-2, 0], [0, 2]],
    cluster_std=[0.4, 0.1, 0.3], 
    random_state=0)
plt.figure(figsize=(8, 6))
plt.scatter(data_5[:, 0], data_5[:, 1], c='blue', s=10)

# generate data_6: different cluster densities
data_6, labels_6 = make_blobs(
    n_samples=750, 
    centers=[[1, 1], [-1, -1], [1, -1]], 
    cluster_std=0.4)
plt.figure(figsize=(8, 6))
plt.scatter(data_6[:, 0], data_6[:, 1], c='blue', s=10)

# do kmeans on the original data_6
kmeans = KMeans(init='k-means++', n_clusters=3)
kmeans_plot(data=data_6, model=kmeans)

# re-scale one dimension of data 6 such that kmeans can not
# cluster into 3 groups as expected. 
data_6[:, 0] /= 4
kmeans = KMeans(init='k-means++', n_clusters=3)
kmeans_plot(data=data_6, model=kmeans)


# 2. Unsupervised Learning: Density Based Clustering
# plot the elbow of k-dist for data 1
kdist_plot(data_1, scaling=False)

metrics = ['euclidean', 'manhattan', 'cosine']
eps, k = 0.8, 4
for scaling in [False, True]:
    for metric in metrics:
        # get title
        title = 'DBSCAN on data 1 with eps={}, min_samples={},         {} metric, '.format(eps, k, metric)
        if scaling:
            title += 'scaling'
        else:
            title += 'no scaling'

        # build model
        dbscan = DBSCAN(eps=eps, min_samples=k, metric=metric)
        
        # plot and evaluate
        pred = dbscan_plot(data_1, dbscan, scaling, title).astype(np.int32)
        evaluate(data_1, labels_1, pred, title)

# plot the elbow of k-dist for data 2
kdist_plot(data_2, k=4, scaling=False)

eps, k = 0.06, 4
for scaling in [False, True]:
    for metric in metrics:
        # get title
        title = 'DBSCAN on data 2 with eps={}, min_samples={},         {} metric, '.format(eps, k, metric)
        if scaling:
            title += 'scaling'
        else:
            title += 'no scaling'

        # build model
        dbscan = DBSCAN(eps=eps, min_samples=k, metric=metric)
        
        # plot and evaluate
        pred = dbscan_plot(data_2, dbscan, scaling, title).astype(np.int32)
        evaluate(data_2, labels_2, pred, title)

# plot the elbow of k-dist for data 3
kdist_plot(data_3, k=4, scaling=False)

eps, k = 0.45, 4
for scaling in [False, True]:
    for metric in metrics:
        # get title
        title = 'DBSCAN on data 3 with eps={}, min_samples={},         {} metric, '.format(eps, k, metric)
        if scaling:
            title += 'scaling'
        else:
            title += 'no scaling'

        # build model
        dbscan = DBSCAN(eps=eps, min_samples=k, metric=metric)
        
        # plot and evaluate
        pred = dbscan_plot(data_3, dbscan, scaling, title).astype(np.int32)
        evaluate(data_3, labels_3, pred, title)

# plot the elbow of k-dist for data 4
kdist_plot(data_4, k=4, scaling=False)

eps, k = 0.15, 4
for scaling in [False, True]:
    for metric in metrics:
        # get title
        title = 'DBSCAN on data 4 with eps={}, min_samples={},         {} metric, '.format(eps, k, metric)
        if scaling:
            title += 'scaling'
        else:
            title += 'no scaling'

        # build model
        dbscan = DBSCAN(eps=eps, min_samples=k, metric=metric)
        
        # plot and evaluate
        pred = dbscan_plot(data_4, dbscan, scaling, title).astype(np.int32)
        evaluate(data_4, labels_4, pred, title)

# plot the elbow of k-dist for data 5
kdist_plot(data_5, k=4, scaling=False)

eps, k = 0.22, 4
for scaling in [False, True]:
    for metric in metrics:
        # get title
        title = 'DBSCAN on data 5 with eps={}, min_samples={},         {} metric, '.format(eps, k, metric)
        if scaling:
            title += 'scaling'
        else:
            title += 'no scaling'

        # build model
        dbscan = DBSCAN(eps=eps, min_samples=k, metric=metric)
        
        # plot and evaluate
        pred = dbscan_plot(data_5, dbscan, scaling, title).astype(np.int32)
        evaluate(data_5, labels_5, pred, title)

# plot the elbow of k-dist for data 6
kdist_plot(data_6, k=4, scaling=False)

eps, k = 0.08, 4
for scaling in [False, True]:
    for metric in metrics:
        # get title
        title = 'DBSCAN on data 6 with eps={}, min_samples={},         {} metric, '.format(eps, k, metric)
        if scaling:
            title += 'scaling'
            data_1 = StandardScaler().fit_transform(data_1)
        else:
            title += 'no scaling'

        # build model
        dbscan = DBSCAN(eps=eps, min_samples=k, metric=metric)
        
        # plot and evaluate
        pred = dbscan_plot(data_6, dbscan, scaling, title).astype(np.int32)
        evaluate(data_6, labels_6, pred, title)


url = 'https://gitlab.com/yuxuan.chen/storage/'\
    '-/tree/master/MLDS-2019/week5/iris.csv'
iris = pd.read_csv(url)

species = {'Iris-setosa': 0, 'Iris-versicolor': 1, 'Iris-virginica': 2}

# PCA analysis
pca = PCA(n_components=2)
pca.fit(iris.drop('Species', axis=1))

# using PCA to reduce dimension
# from 4 attributes to 2: PCA1, PCA2
iris_pca = pca.transform(iris.drop('Species', axis=1))
iris_pca = pd.DataFrame(iris_pca, columns=['PC1', 'PC2'])

# replace former columns and standardization
f = lambda x: round(x, 1)
iris_pca = iris_pca.applymap(f)
iris_pca = pd.concat([iris_pca, iris['Species']], axis=1)

# plot iris dataset after PCA
plt.figure(figsize=(10,6))
plt.title('PCA Visualization for iris dataset of k=2')
sns.stripplot(x='PC1', y='PC2', hue='Species', data=iris_pca)

iris_pca = iris_pca.replace({'Species': species})
print(iris_pca.head(5))

# plot the elbow of k-dist for iris
iris_data = np.array([[x, y] for x, y in zip(
        iris_pca['PC1'], iris_pca['PC2'])])
iris_labels = iris_pca['Species']
kdist_plot(iris_data, k=4, scaling=False)

eps, k = 2.4, 4
for scaling in [False, True]:
    for metric in ['euclidean']:
        # get title
        title = 'DBSCAN on iris data with eps={}, min_samples={},         {} metric, '.format(eps, k, metric)
        if scaling:
            title += 'scaling'
            iris_data = StandardScaler().fit_transform(iris_data)
        else:
            title += 'no scaling'

        # build model
        dbscan = DBSCAN(eps=eps, min_samples=k, metric=metric)
        
        # plot and evaluate
        pred = dbscan_plot(iris_data, dbscan, scaling, title).astype(np.int32)
        evaluate(iris_data, iris_labels, pred, title)


# Extend the Data (2)
from mpl_toolkits.mplot3d import Axes3D
import somoclu

# generate data_7: 3-d clusters
centers = [[4, 0, 0], [0, 4, 0], [0, 0, 4]]
data_7, labels_7 = make_blobs(n_samples=150,
                              n_features=3, 
                              centers=centers,
                              cluster_std=0.3, 
                              random_state=13)

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_7[:, 0], data_7[:, 1], data_7[:, 2], c=labels_7)
plt.show()

# generate data_8: 4-d clusters
data_8, labels_8 = make_blobs(n_samples=300,
                              n_features=4, 
                              centers=4,
                              cluster_std=0.8, 
                              random_state=13)

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_8[:, 0], data_8[:, 1], data_8[:, 2], c=labels_8)
plt.show()

# generate data_9: 5-d clusters
data_9, labels_9 = make_blobs(n_samples=300,
                              n_features=5, 
                              centers=5,
                              cluster_std=0.8, 
                              random_state=13)

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_9[:, 0], data_9[:, 1], data_9[:, 2], c=labels_9)
plt.show()


# 4. Unsupervised Learning: Self-Organizing Maps
# train som on data 7
n_rows, n_columns = 100, 160
som = somoclu.Somoclu(n_columns, n_rows, data=data_7)
get_ipython().run_line_magic('time', 'som.train()')

som.view_umatrix(
    bestmatches=True, 
    bestmatchcolors=labels_7, 
    labels=labels_7)

# extract cluster
som.cluster()
pred = [
    som.clusters[som.bmus[i, 1], som.bmus[i, 0]] \
    for i in range(data_7.shape[0])
]

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_7[:, 0], data_7[:, 1], data_7[:, 2], c=pred)
plt.show()

title = 'SOM on data 7'
evaluate(data_7, labels_7, pred, title)

# train som on data 7 with 200*300 grid
n_rows, n_columns = 200, 300
som = somoclu.Somoclu(n_columns, n_rows, data=data_7)
get_ipython().run_line_magic('time', 'som.train()')

som.view_umatrix(
    bestmatches=True, 
    bestmatchcolors=labels_7, 
    labels=labels_7)

# extract cluster
som.cluster()
pred = [
    som.clusters[som.bmus[i, 1], som.bmus[i, 0]] \
    for i in range(data_7.shape[0])
]

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_7[:, 0], data_7[:, 1], data_7[:, 2], c=pred)
plt.show()

title = 'SOM on data 7 with 200*300 grid'
evaluate(data_7, labels_7, pred, title)

# train som on data 7 with 'hexagonal' grid
n_rows, n_columns = 100, 160
som = somoclu.Somoclu(
    n_columns, n_rows, data=data_7, gridtype='hexagonal')
get_ipython().run_line_magic('time', 'som.train()')

som.view_umatrix(
    bestmatches=True, 
    bestmatchcolors=labels_7, 
    labels=labels_7)

# extract cluster
som.cluster()
pred = [
    som.clusters[som.bmus[i, 1], som.bmus[i, 0]] \
    for i in range(data_7.shape[0])
]

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_7[:, 0], data_7[:, 1], data_7[:, 2], c=pred)
plt.show()

title = 'SOM on data 7 with 200*300 grid'
evaluate(data_7, labels_7, pred, title)

# train som on data 8
n_rows, n_columns = 100, 160
som = somoclu.Somoclu(n_columns, n_rows, data=data_8)
get_ipython().run_line_magic('time', 'som.train()')

som.view_umatrix(
    bestmatches=True, 
    bestmatchcolors=labels_8, 
    labels=labels_8)

# extract cluster
som.cluster()
pred = [
    som.clusters[som.bmus[i, 1], som.bmus[i, 0]] \
    for i in range(data_8.shape[0])
]

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_8[:, 0], data_8[:, 1], data_8[:, 2], c=pred)
plt.show()

title = 'SOM on data 8'
evaluate(data_8, labels_8, pred, title)

# train som on data 9
n_rows, n_columns = 100, 160
som = somoclu.Somoclu(n_columns, n_rows, data=data_9)
get_ipython().run_line_magic('time', 'som.train()')

som.view_umatrix(
    bestmatches=True, 
    bestmatchcolors=labels_9, 
    labels=labels_9)

# extract cluster
som.cluster()
pred = [
    som.clusters[som.bmus[i, 1], som.bmus[i, 0]] \
    for i in range(data_9.shape[0])
]

# 3-d plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_9[:, 0], data_9[:, 1], data_9[:, 2], c=pred)
plt.show()

title = 'SOM on data 9'
evaluate(data_9, labels_9, pred, title)

# train som on data 2
n_rows, n_columns = 100, 160
som = somoclu.Somoclu(n_columns, n_rows, data=data_2)
get_ipython().run_line_magic('time', 'som.train()')

som.view_umatrix(
    bestmatches=True, 
    bestmatchcolors=labels_2, 
    labels=labels_2)

# extract cluster
som.cluster()
pred = [
    som.clusters[som.bmus[i, 1], som.bmus[i, 0]] \
    for i in range(data_2.shape[0])
]

# plot
plt.figure(figsize=(8, 6))
plt.scatter(data_2[:, 0], data_2[:, 1], c=pred, s=10)
plt.show()

title = 'SOM on data 2'
evaluate(data_2, labels_2, pred, title)

# train som on iris_data
n_rows, n_columns = 100, 160
som = somoclu.Somoclu(n_columns, n_rows, data=iris_data)
get_ipython().run_line_magic('time', 'som.train()')

som.view_umatrix(
    bestmatches=True, 
    bestmatchcolors=iris_labels, 
    labels=iris_labels)

# extract cluster
som.cluster()
pred = [
    som.clusters[som.bmus[i, 1], som.bmus[i, 0]] \
    for i in range(iris_data.shape[0])
]

# plot
plt.figure(figsize=(8, 6))
plt.scatter(iris_data[:, 0], iris_data[:, 1], c=pred, s=10)
plt.show()

title = 'SOM on iris data'
evaluate(iris_data, iris_labels, pred, title)